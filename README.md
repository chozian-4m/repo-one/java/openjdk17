# Description

Oracle OpenJDK17. More information can be found [here](https://jdk.java.net/17/).

# Environment Variables
This image has the following environment variables which can be used to customize behavior:
- **JAVA_HOME**: /opt/jdk-17.0.2